package com.gideon.create;

import com.gideon.entity.ChartPosition;
import com.gideon.entity.LineChart;
import com.gideon.entity.PieChart;
import com.gideon.utils.ChartUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author GideonYeung
 * @date 2020/12/3 17:14
 */
public class ChartTest {

    public static void main(String[] args) throws IOException {
        XSSFWorkbook workbook = createLineChart();

        //-------------------------下载--------------------------
        FileOutputStream fileOut = null;
        try {
            // 将输出写入excel文件
            String filename = UUID.randomUUID() + ".xlsx";
            fileOut = new FileOutputStream("D:\\excel_demo\\" + filename);
            workbook.write(fileOut);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workbook.close();
            if (fileOut != null) {
                fileOut.close();
            }
        }
    }

    /**
     * 这种是直接用当数据填充
     */
    public static XSSFWorkbook createLineChart() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        //-------------------------折线图--------------------------
        List<LineChart> lineCharts = initLineChart();
        ChartPosition chartPosition;
        //最后一列
        int lastRowNum = sheet.getLastRowNum();
        //图表位置左上角的开始行 +5是为了和上一个图保持五行的距离
        int chartRowStart = lastRowNum + 5;
        //图表位置右上角的开始行
        int chartRowEnd = chartRowStart + 20;
        for (LineChart lineChart : lineCharts) {
            // 图表位置（左上角坐标，右下角坐标） 左上角坐标的（列，行），（右下角坐标）列，行,偏移量均为0
            chartPosition = new ChartPosition()
                    .setRow1(chartRowStart)
                    .setCol1(1)
                    .setRow2(chartRowEnd)
                    .setCol2(lineChart.getXAxisList().size() + 3);
            ChartUtils.createLine(sheet, chartPosition, lineChart);
            chartRowStart = chartRowEnd + 5;
            chartRowEnd = chartRowStart + 25;
        }
        //-------------------------饼图--------------------------
        List<PieChart> pieCharts = initPieChart();
        for (PieChart pieChart : pieCharts) {
            // 图表位置（左上角坐标，右下角坐标） 左上角坐标的（列，行），（右下角坐标）列，行,偏移量均为0
            chartPosition = new ChartPosition()
                    .setRow1(chartRowStart)
                    .setCol1(1)
                    .setRow2(chartRowEnd)
                    .setCol2(pieChart.getDataList().size() + 3);
            ChartUtils.createPie(sheet, chartPosition, pieChart);
            chartRowStart = chartRowEnd + 5;
            chartRowEnd = chartRowStart + 25;
        }
        return workbook;
    }

    public static List<LineChart> initLineChart() {
        List<LineChart> lineCharts = new ArrayList<>();
        lineCharts.add(new LineChart()
                .setChartTitle("包子铺卖出情况")
                .setTitleList(Arrays.asList("馒头", "包子"))
                .setDataList(Arrays.asList(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(5, 4, 3, 2, 1)))
                .setXAxisList(Arrays.asList("1月份", "2月份", "3月份", "4月份", "5月份")));
        //单个折线
        lineCharts.add(new LineChart()
                .setChartTitle("黄金当铺典当情况")
                .setTitleList(Collections.singletonList("黄金"))
                .setDataList(Collections.singletonList(Arrays.asList(11, 21, 31, 41, 51)))
                .setXAxisList(Arrays.asList("1月份", "2月份", "3月份", "4月份", "5月份")));
        return lineCharts;
    }

    public static List<PieChart> initPieChart() {
        List<PieChart> pieCharts = new ArrayList<>();
        pieCharts.add(new PieChart()
                .setTitleName("工资总汇(万)")
                .setTitleList(Arrays.asList("基本工资", "奖金", "绩效", "技能奖金", "出差补贴"))
                .setDataList(Arrays.asList(15, 1, 1, 1, 1)));
        return pieCharts;
    }
}
